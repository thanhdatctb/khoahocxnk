function ULSsP7(){var o=new Object;o.ULSTeamName="CMS";o.ULSFileName="PickerTreeDialog.js";return o;}
// _lcid="1033" _version="14.0.4750"
// _localBinding
// Version: "14.0.4750"
// Copyright (c) Microsoft Corporation.  All rights reserved.
var PickerTreeDlgUrl="/_layouts/PickerTreeView.aspx";
var PickerTreeDlgDimension="resizable:yes;status:no;location:no;menubar:no;help:no;dialogWidth:512px;dialogHeight:547px";
var L_WarningFailedOperation_TEXT="Do you wish to continue?";
var L_NullSelectionText_TEXT="Please select a target or click cancel";
var MAX_SOURCEID_LENGTH=1024;
var IdSeparator=",";
function LaunchPickerTreeDialog(title, text, filter, anchor, siteUrl, select, featureId, errorString, iconUrl, sourceSmtObjectId, callback, scopeToWeb, requiredCT)
{ULSsP7:;
   params=new PickerTreeParameters(sourceSmtObjectId, requiredCT);
   params.FixupParameters();
   var dialogUrl=TrimLastSlash(siteUrl)+PickerTreeDlgUrl+"?title="+title+							"&text="+text+							"&filter="+filter+							"&root="+anchor+							"&selection="+select+							"&featureId="+featureId+							"&errorString="+errorString+							"&iconUrl="+iconUrl+							"&scopeToWeb="+scopeToWeb+							"&requireCT="+params.requiredCT+							"&sourceId="+params.sourceSmtObjectId;
  commonShowModalDialog(dialogUrl, PickerTreeDlgDimension, callback, null);
}
function LaunchPickerTreeDialogSelectUrl(title, text, filter, anchor, siteUrl, selectUrl, featureId, errorString, iconUrl, sourceSmtObjectId, callback, scopeToWeb, requiredCT)
{ULSsP7:;
   params=new PickerTreeParameters(sourceSmtObjectId, requiredCT);
   params.FixupParameters();
   var dialogUrl=TrimLastSlash(siteUrl)+PickerTreeDlgUrl+"?title="+title+							"&text="+text+							"&filter="+filter+							"&root="+anchor+							"&selectionUrl="+selectUrl+							"&featureId="+featureId+							"&errorString="+errorString+							"&iconUrl="+iconUrl+							"&scopeToWeb="+scopeToWeb+							"&requireCT="+params.requiredCT+							"&sourceId="+params.sourceSmtObjectId;
  commonShowModalDialog(dialogUrl, PickerTreeDlgDimension, callback, null);
}
function PickerTreeParameters(sourceSmtObjectId, requiredCT)
{ULSsP7:;
   this.sourceSmtObjectId=sourceSmtObjectId;
   this.requiredCT=requiredCT;
   this.FixupParameters=FixupPickerTreeParameters;
}
function FixupPickerTreeParameters()
{ULSsP7:;
   var sources=null;
   if(this.sourceSmtObjectId !=null && this.sourceSmtObjectId.length > MAX_SOURCEID_LENGTH)
   {
	  sources=this.sourceSmtObjectId.split(IdSeparator);
	  if(sources.length > 1)
	  {
		 this.sourceSmtObjectId=sources[0];
	  }
   }
   if (this.requiredCT==null || this.requiredCT==undefined)
   {
	   this.requiredCT='';
   }
}
function LaunchVariationPickerTreeDialog(siteUrl, callback)
{ULSsP7:;
   var dialogUrl=TrimLastSlash(siteUrl)+PickerTreeDlgUrl+"?title=VariationBrowseTitle"+							"&text=VariationBrowseRootWeb"+							"&filter=websOnly"+							"&returnSiteRelativeUrl=true";
  commonShowModalDialog(dialogUrl, PickerTreeDlgDimension, callback, null);
}
function TrimLastSlash(siteUrl)
{ULSsP7:;
	return (siteUrl.charAt(siteUrl.length - 1)=="/")? siteUrl.substring(0,(siteUrl.length - 1)) : siteUrl;
}
function HandleOkReturnValues(strDlgReturnValue, strDlgReturnErr)
{ULSsP7:;
	if (strDlgReturnValue[0].indexOf("Error:") >=0)
	{
		alert( strDlgReturnValue[0].slice(7));
	}
	else
	{
		if(strDlgReturnErr.indexOf("Error:") >=0)
		{
			var promptUser=strDlgReturnErr.slice(7)+". "+L_WarningFailedOperation_TEXT;
			if(confirm(promptUser)==0)
				return false;
		}
		else if(strDlgReturnErr.indexOf("Alert:") >=0)
		{
			alert(strDlgReturnErr.slice(7));
		}
		setModalDialogReturnValue(window,strDlgReturnValue);
		window.top.close();
	}
	return false;
}

