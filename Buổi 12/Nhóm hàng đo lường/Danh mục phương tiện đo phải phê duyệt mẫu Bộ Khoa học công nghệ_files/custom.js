(function ($){
  $(document).ready(function(){
    $('#chuyen_muc .uk-position-cover').click(function(){
      var url = $('#url').val();
      var term_id = $(this).data('id');
      console.log(term_id);
      $('#chuyen_muc .uk-position-cover').removeClass('uk-active');
      $(this).addClass('uk-active');
      $.ajax({
        type : "post",
        dataType : "json",
        url : url ,
        data : {
                       action: "chuyen_muc", //Tên action
                       term_id: term_id,
                },
        context: this,
                beforeSend: function(){
                                //Làm gì đó trước khi gửi dữ liệu vào xử lý
                $('.content_change .uk-position-cover').removeClass('uk-hidden');
                },
        success: function(response) {
               //Làm gì đó khi dữ liệu đã được xử lý
              if(response.success) {
              $('.content_change .uk-slideshow-items').html(response.data);
              $('.content_change .uk-position-cover').addClass('uk-hidden');
                    }
              else {
              alert('Đã có lỗi xảy ra');
                  }
              },
        error: function( jqXHR, textStatus, errorThrown ){
                                                       //Làm gì đó khi có lỗi xảy ra
                    console.log( 'The following error occured: ' + textStatus, errorThrown );
                                                   }
      })
      return false;
    });
  });
})(jQuery);
